# -*- coding: utf-8 -*-
import cv2
from readfile import readfile,createFolder
from part1 import part1
from part2 import part2
import os

flashpath=["images/flash.jpg","images/teddy.jpg","images/teddyFlash.jpg","images/teapotFlash.jpg"]
noflashpath=["images/no-flash.jpg","images/teddyno.jpg","images/teddyNo-flash.jpg","images/teapotNo-flash.jpg"]
channels=["RGB","HSV","LAB"]

def main():
    createFolder("results/part1")
    createFolder("results/part2")


    for i in range(1,len(flashpath)+1):
        flash, noflash = readfile(flashpath[i-1], noflashpath[i-1])

        part1(flash,noflash,"image"+str(i))

        for k in range(len(channels)):

            part2(flash, noflash, channels[k], 5 , 0.1,"image"+str(i))
            part2(flash, noflash, channels[k], 15, 0.1,"image"+str(i))
            part2(flash, noflash, channels[k], 5 , 1000, "image" + str(i))


main()



cv2.waitKey(0)
cv2.destroyAllWindows()