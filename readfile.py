import cv2
import numpy as np
import os



def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print('Error: Creating directory. ' + directory)


def readfile(flashpath, noflashpath):
    flash_rgb = cv2.imread(flashpath)/255
    no_flash_rgb = cv2.imread(noflashpath)/255


    return flash_rgb, no_flash_rgb



# transform to np.uint8 format
def uint8(image):

    return np.uint8(np.clip(image * 255, 0, 255))


def normalization(image):
    cv2.normalize(image, image, 0, 255, cv2.NORM_MINMAX)
    return image