import numpy as np
import cv2
import os
from readfile import uint8,normalization,createFolder

def part1(flash_rgb,noflash_rgb,imagename):
    flash_gray = cv2.cvtColor(uint8(flash_rgb), cv2.COLOR_BGR2GRAY)/255
    noflash_gray = cv2.cvtColor(uint8(noflash_rgb), cv2.COLOR_BGR2GRAY)/255
    path = "results/part1/"+str(imagename)
    createFolder(path)
    colur_layer_flash = np.divide(flash_rgb, flash_gray[:, :, None], out=np.zeros_like(flash_rgb),
                                  where=flash_gray[:, :, None] != 0)
    colur_layer_noflash = np.divide(noflash_rgb, noflash_gray[:, :, None], out=np.zeros_like(noflash_rgb),
                                  where=noflash_gray[:, :, None] != 0)

    cv2.imwrite(os.path.join(path, "colur_layer_flash.jpg"), uint8(colur_layer_flash))
    cv2.imwrite(os.path.join(path, "colur_layer_Noflash.jpg"), uint8(colur_layer_noflash))


    bilateral_flash = cv2.bilateralFilter(uint8(flash_gray),3,100,5)
    cv2.imwrite(os.path.join(path,"bilateral_flash.jpg"), bilateral_flash)

    bilateral_noflash = cv2.bilateralFilter(uint8(noflash_gray),3,100,5)
    cv2.imwrite(os.path.join(path,"bilateral_no-flash.jpg"), bilateral_noflash)

    edge = np.divide(flash_gray , bilateral_flash/255)
    cv2.imwrite(os.path.join(path,"flash_bilateral_edges.jpg"),uint8(edge))

    edge_noflash= np.multiply(edge,bilateral_noflash/255,out=np.zeros_like(edge), where=bilateral_flash / 255!=0)
    result = np.multiply(colur_layer_flash,edge_noflash[:,:,None])
    cv2.imwrite(os.path.join(path,"reconstruction.jpg"),normalization(result))

    # part 1 bitti :)

